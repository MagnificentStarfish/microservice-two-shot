import { useEffect, useState } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList'
import ShoeList from './ShoeList';
import NewHat from './NewHat'
import NewShoe from './NewShoe';

function App() {
  const[hats, setHats] = useState([])
  const[shoes, setShoes] = useState([])
  const[location, setLocation] = useState([])
  const[bin, setBin] = useState([])
  const getHats = async () => {
    const hatResponse = await fetch('http://localhost:8090/api/hats/');

    if (hatResponse.ok) {
      const data = await hatResponse.json();
      const hats = data.hats;
      setHats(hats);
      console.log(data);
    }
  }
  const getShoes = async () => {
    const shoeResponse = await fetch('http://localhost:8080/api/shoes/');

    if (shoeResponse.ok) {
      const data = await shoeResponse.json();
      const shoes = data.shoes;
      setShoes(shoes);
      console.log(data);
    }
  }

  const getBins = async () => {
    const binResponse = await fetch('http://localhost:8100/api/bins/');

    if (binResponse.ok) {
      const data = await binResponse.json();
      const bin = data.bins;
      setBin(bin);

    }
  }
  const getLocation = async () => {
    const locationResponse = await fetch('http://localhost:8100/api/locations/');

    if (locationResponse.ok) {
      const data = await locationResponse.json();
      const location = data.locations;
      setLocation(location);
    }
  }


useEffect( () => {getHats(); getLocation(); getBins(); getShoes()}, [])
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" >
            <Route path="" element={<HatsList hats={hats} getHats={getHats}/>} />
            <Route path="new" element={<NewHat locations = {location} getHats={getHats} />} />
          </Route>
          <Route path="/shoes" >
            <Route path="" element={<ShoeList shoes={shoes} getShoes={getShoes}/>} />
            <Route path="new" element={<NewShoe bins={bin} getShoes={getShoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
