function HatsList({hats, getHats}) {
    if (hats === undefined) {
      return null;
    }
    return (
      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
                <th>Number</th>
                <th>Style</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {hats.map(hat => {
              return (
                <tr key={hat.id}>
                    <td>{ hat.id} </td>
                    <td>{ hat.style }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.color }</td>
                    <td>{ hat.location }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }

  export default HatsList;
