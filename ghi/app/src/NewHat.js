import React, {useState } from 'react'

export default function NewHat({locations, getHats}) {
    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
  }

    const handleFabricChange = (event) => {
        const value = event.target.value
        setFabric(value)
  }

    const handleStyleChange = (event) => {
        const value = event.target.value
        setStyle(value)
  }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
  }
    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value)

  }
  const handleLocationChange = (event) => {
    const value = event.target.value
    setLocation(value)
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.name = name;
    data.fabric = fabric;
    data.style = style;
    data.color = color;
    data.location = location;
    data.picture = picture;

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newHat = await response.json();
      console.log(newHat);

      setName('')
      setFabric('')
      setStyle('')
      setColor('')
      setPicture('')
      setLocation('')
      getHats()
    }
  }

  // How can we refactor these handleChange methods to make
  // a single method, like the ConferenceForm above?

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control" value={name} />
                        <label htmlFor="style_name">Style</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={style} onChange={handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
                        <label htmlFor="name">Style</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" value={picture} />
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select required onChange={handleLocationChange} name="location" id="location" className="form-select" value={location}>
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.href}>
                                        {location.closet_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
)
}
