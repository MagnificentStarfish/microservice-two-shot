import React, {useState } from 'react'

export default function NewShoe({bins, getShoes}) {
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');


    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
  }

    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
  }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
  }
    const handlePictureChange = (event) => {
        const value = event.target.value
        setPicture(value)

  }
  const handleBinChange = (event) => {
    const value = event.target.value
    setBin(value)
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = {};
    data.name = name;
    data.manufacturer = manufacturer;
    data.color = color;
    data.bin = bin;
    data.picture = picture;

    const hatUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    }
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      const newShoe = await response.json();
      console.log(newShoe);

      setName('')
      setManufacturer('')
      setColor('')
      setPicture('')
      setBin('')
      getShoes()
    }
  }

  // How can we refactor these handleChange methods to make
  // a single method, like the ConferenceForm above?

  return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                        <label htmlFor="fabric">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" value={manufacturer} />
                        <label htmlFor="style_name">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" value={picture} />
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select required onChange={handleBinChange} name="bin" id="bin" className="form-select" value={bin}>
                            <option value="">Choose a bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Shoe Me!</button>
                </form>
            </div>
        </div>
    </div>
)
}
