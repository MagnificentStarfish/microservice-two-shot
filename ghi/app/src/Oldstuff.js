//   const fetchData = async () => {
//     const url = 'http://localhost:8100/api/locations/';

//     const response = await fetch(url);

//     if (response.ok) {
//       const data = await response.json();
//       setStates(data.states);
//     }


//   useEffect(() => {
//     fetchData();
//   }, []);


return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" />
              <label htmlFor="name">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input value={style} onChange={handleStyleChange} placeholder="Style" required type="text" name="style" id="style" className="form-control" />
              <label htmlFor="name">Style</label>
            </div>
            <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="room_count">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input value={picture} onChange={handlePictureChange} placeholder="Picture URL" required type="text" name="picture" id="picture" className="form-control" />
              <label htmlFor="city">Picture</label>
            </div>
            <div className="mb-3">
              <select value={location} onChange={handleLocationChange} required name="location" id="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                  return (
                    <option key={location.id} value={location.href}>
                      {location.id}
                    </option>
                  );
                })}
              </select>
            </div>
            <button className="btn btn-primary">Let's Go!</button>
          </form>
        </div>
      </div>
    </div>
  );
}
}
