function ShoeList({shoes, getshoes}) {
    if (shoes === undefined) {
      return null;
    }
    return (
      <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
                <th>Number</th>
                <th>Style</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Location</th>
            </tr>
          </thead>
          <tbody>
            {shoes.map(shoe => {
              return (
                <tr key={shoe.id}>
                    <td>{ shoe.id} </td>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.color }</td>
                    <td>{ shoe.picture }</td>
                    <td>{ shoe.bin }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }

  export default ShoeList;
