import React, {useState} from 'react'

export default function HatForm({locations, getHats}) {
    const [fabric, setFabric] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [picture, setPicture] = useState('');
    const [location, setLocation] = useState('');

    const handleFabricChange = (e) => {
        const value = e.target.value
        setFabric(value)
    }
    const handleNameChange = (e) => {
        const value = e.target.value
        setName(value)
    }
    const handleColorChange = (e) => {
        const value = e.target.value
        setColor(value)
    }
    const handlePictureChange = (e) => {
        const value = e.target.value
        setPicture(value)
    }
    const handleLocationChange = (e) => {
        const value = e.target.value
        setLocation(value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.fabric = fabric;
        data.style_name = name;
        data.color = color;
        data.picture_url = picture;
        data.location = location;

        console.log(data);

        const hatUrl = `http://localhost:8090/api/hats/`
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'applications/json'
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json();
            setName('')
            setFabric('')
            setColor('')
            setPicture('')
            setLocation('')
            getHats()
        }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control" value={name} />
                            <label htmlFor="style_name">Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" value={picture} />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select required onChange={handleLocationChange} name="location" id="location" className="form-select" value={location}>
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
